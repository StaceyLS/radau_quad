sets n "colocation elements" /node1 * node60/
     p "point type" /end, mid/
     i "trunk coordinates" /x,y,th,th_tail/
     i_angles(i) /th,th_tail/
     j "joints" /shoulder,elbow,hip,knee/
     j1(j) /shoulder,hip/
     j2(j) /elbow,knee/
     jf(j) /shoulder,elbow/
     jh(j) /hip,knee/
     l "limbs" /upperarm,forearm,forefoot,thigh,calf,hindfoot/
     l2(l) /forearm,calf/
     feet(l) "feet" /forefoot,hindfoot/
     s "sides" /R/
     sgn "sign" /ps,ng/
     gr /contact,friction,slip_pos,slip_neg/
     end /A,B/
     tr "trig functions" /sin,cos/

     seed /s1*s500/;

singleton set friction /high/;
scalar point /1/
tail_bool /1/;

Scalars
g /9.81/
uground /1.2/
pi /3.14159265359/

m_t /4.5/
len_t /0.5/
clen_t /0.375/
In_t
m_body

m_tail /0.5/
len_tail /0.5/
clen_tail
In_tail
;

In_t = m_t*(len_t**2)/12;
clen_tail = len_tail/2;
In_tail = m_tail*(len_tail**2)/12;

Parameters
m(l) "mass" /upperarm 1.5, forearm 1.5, forefoot 0, thigh 1.5, calf 1.5, hindfoot 0/
len(l) "length" /upperarm 0.2, forearm 0.25, forefoot 0, thigh 0.2, calf 0.3, hindfoot 0/
clen(l)
In(l)
;

clen(l) = len(l)/2;
In(l) = m(l)*(len(l)**2)/12;
m_body = m_t + m_tail + card(j)*sum(l, m(l));

variables
q_t(n,p,i) "torso coordinates"
dq_t(n,p,i)
ddq_t(n,p,i)

th_joints(n,p,j,s) "joint angles"
dth_joints(n,p,j,s) "joint angles"
ddth_joints(n,p,j,s)

tau(n,p,j,s)
tau_tail(n,p);

positive variables
h_global
h(n)
GRF(n,p,l,s,i,sgn)
;

h.up(n) = 0.012;
h.lo(n) = 0.001;
h.l(n) = 0.005;

scalars
Tmax /56/
w_noload /68/
;

*angle bounds
q_t.up(n,"end","th") = pi/2;
q_t.lo(n,"end","th") = -pi/2;

q_t.up(n,"end","th_tail") = pi/2;
q_t.lo(n,"end","th_tail") = -pi;

th_joints.up(n,"end","hip",s) = pi/2;
th_joints.lo(n,"end","hip",s) = -pi/2;

th_joints.up(n,"end","shoulder",s) = pi/2;
th_joints.lo(n,"end","shoulder",s) = -pi/2;

th_joints.up(n,"end","knee",s) = 0.1;
th_joints.lo(n,"end","knee",s) = -pi/2;

th_joints.up(n,"end","elbow",s) = pi/2;
th_joints.lo(n,"end","elbow",s) = 0;

*TIME---------------------------------------------------------------------------
Equations
time_min(n,p)
time_max(n,p)
;

time_min(n,p).. h(n) =g= 0.8*h_global;
time_max(n,p).. h(n) =l= 1.2*h_global;

*MOTOR MODEL--------------------------------------------------------------------
Equations
tau_min(n,p,j,s)
tau_max(n,p,j,s)

tau_tail_min(n,p)
tau_tail_max(n,p)

;

tau_min(n,p,j,s).. tau(n,p,j,s) =g= -Tmax - (Tmax/w_noload)*dth_joints(n,p,j,s);
tau_max(n,p,j,s).. tau(n,p,j,s) =l= Tmax - (Tmax/w_noload)*dth_joints(n,p,j,s);

tau_tail_min(n,p).. tau_tail(n,p) =g= -Tmax - (Tmax/w_noload)*dq_t(n,p,"th_tail");
tau_tail_max(n,p).. tau_tail(n,p) =l= Tmax - (Tmax/w_noload)*dq_t(n,p,"th_tail");

tau.up(n,p,j,s) = Tmax;
tau.lo(n,p,j,s) = -Tmax;

tau_tail.up(n,p) = Tmax;
tau_tail.lo(n,p) = -Tmax;

*SIN AND COS--------------------------------------------------------------------
variables
trig_t(n,p,i,tr)
trig(n,p,j,s,tr)
;

Equations
get_sin_t(n,p,i)
get_cos_t(n,p,i)
get_sin(n,p,j,s)
get_cos(n,p,j,s)

;

get_sin_t(n,p,i)$i_angles(i).. trig_t(n,p,i,"sin") =e= q_t(n,p,i) - power(q_t(n,p,i),3)/fact(3) + power(q_t(n,p,i),5)/fact(5) - power(q_t(n,p,i),7)/fact(7)
                                                         + power(q_t(n,p,i),9)/fact(9) - power(q_t(n,p,i),11)/fact(11) + power(q_t(n,p,i),13)/fact(13)
                                                         - power(q_t(n,p,i),15)/fact(15) + power(q_t(n,p,i),17)/fact(17) - power(q_t(n,p,i),19)/fact(19);
get_cos_t(n,p,i)$i_angles(i).. trig_t(n,p,i,"cos") =e= 1 - power(q_t(n,p,i),2)/fact(2) + power(q_t(n,p,i),4)/fact(4) - power(q_t(n,p,i),6)/fact(6)
                                                         + power(q_t(n,p,i),8)/fact(8) - power(q_t(n,p,i),10)/fact(10) + power(q_t(n,p,i),12)/fact(12)
                                                         - power(q_t(n,p,i),14)/fact(14) + power(q_t(n,p,i),16)/fact(16) - power(q_t(n,p,i),18)/fact(18);

get_sin(n,p,j,s).. trig(n,p,j,s,"sin") =e= th_joints(n,p,j,s) - power(th_joints(n,p,j,s),3)/fact(3) + power(th_joints(n,p,j,s),5)/fact(5) - power(th_joints(n,p,j,s),7)/fact(7)
                                                         + power(th_joints(n,p,j,s),9)/fact(9) - power(th_joints(n,p,j,s),11)/fact(11) + power(th_joints(n,p,j,s),13)/fact(13)
                                                         - power(th_joints(n,p,j,s),15)/fact(15) + power(th_joints(n,p,j,s),17)/fact(17) - power(th_joints(n,p,j,s),19)/fact(19);
get_cos(n,p,j,s).. trig(n,p,j,s,"cos") =e= 1 - power(th_joints(n,p,j,s),2)/fact(2) + power(th_joints(n,p,j,s),4)/fact(4) - power(th_joints(n,p,j,s),6)/fact(6)
                                                         + power(th_joints(n,p,j,s),8)/fact(8) - power(th_joints(n,p,j,s),10)/fact(10) + power(th_joints(n,p,j,s),12)/fact(12)
                                                         - power(th_joints(n,p,j,s),14)/fact(14) + power(th_joints(n,p,j,s),16)/fact(16) - power(th_joints(n,p,j,s),18)/fact(18);

*DYNAMICS-----------------------------------------------------------------------
$include "dynamics.txt";

*interpolation
Equations
interp_q_t(n,i)
interp_dq_t(n,i)

interp_th_joints(n,j,s)
interp_dth_joints(n,j,s)

interp_tau(n,j,s)
interp_tau_tail(n)

interp_GRF_x(n,l,s,sgn)
interp_GRF_y(n,l,s)
;

interp_q_t(n,i)$(ord(n) gt 1).. q_t(n,"mid",i) =e= 0.5*(q_t(n,"end",i) + q_t(n-1,"end",i)) + 0.125*h(n)*(dq_t(n,"end",i) - dq_t(n-1,"end",i));
interp_dq_t(n,i)$(ord(n) gt 1).. dq_t(n,"mid",i) =e= 0.5*(dq_t(n,"end",i) + dq_t(n-1,"end",i)) + 0.125*h(n)*(ddq_t(n,"end",i) - ddq_t(n-1,"end",i));

interp_th_joints(n,j,s)$(ord(n) gt 1).. th_joints(n,"mid",j,s) =e= 0.5*(th_joints(n,"end",j,s) + th_joints(n-1,"end",j,s)) + 0.125*h(n)*(dth_joints(n,"end",j,s) - dth_joints(n-1,"end",j,s));
interp_dth_joints(n,j,s)$(ord(n) gt 1).. dth_joints(n,"mid",j,s) =e= 0.5*(dth_joints(n,"end",j,s) + dth_joints(n-1,"end",j,s)) + 0.125*h(n)*(ddth_joints(n,"end",j,s) - ddth_joints(n-1,"end",j,s));

interp_tau(n,j,s)$(ord(n) gt 1).. tau(n,"mid",j,s) =e= 0.5*(tau(n,"end",j,s) + tau(n-1,"end",j,s));
interp_tau_tail(n)$(ord(n) gt 1).. tau_tail(n,"mid") =e= 0.5*(tau_tail(n,"end") + tau_tail(n-1,"end"));

interp_GRF_x(n,l,s,sgn)$(ord(n) gt 1 and feet(l)).. GRF(n,"mid",l,s,"x",sgn) =e= 0.5*(GRF(n,"end",l,s,"x",sgn) + GRF(n-1,"end",l,s,"x",sgn));
interp_GRF_y(n,l,s)$(ord(n) gt 1 and feet(l)).. GRF(n,"mid",l,s,"y","ps") =e= 0.5*(GRF(n,"end",l,s,"y","ps") + GRF(n-1,"end",l,s,"y","ps"));

*defects
Equations
pos_defect_t(n,i)
vel_defect_t(n,i)

pos_defect_j(n,j,s)
vel_defect_j(n,j,s)
;

pos_defect_t(n,i)$(ord(n) gt 1).. q_t(n,"end",i) =e= q_t(n-1,"end",i) + h(n)*(dq_t(n-1,"end",i) + 4*dq_t(n,"mid",i) + dq_t(n,"end",i))/6;
vel_defect_t(n,i)$(ord(n) gt 1).. dq_t(n,"end",i) =e= dq_t(n-1,"end",i) + h(n)*(ddq_t(n-1,"end",i) + 4*ddq_t(n,"mid",i) + ddq_t(n,"end",i))/6;

pos_defect_j(n,j,s)$(ord(n) gt 1).. th_joints(n,"end",j,s) =e= th_joints(n-1,"end",j,s) + h(n)*(dth_joints(n-1,"end",j,s) + 4*dth_joints(n,"mid",j,s) + dth_joints(n,"end",j,s))/6;
vel_defect_j(n,j,s)$(ord(n) gt 1).. dth_joints(n,"end",j,s) =e= dth_joints(n-1,"end",j,s) + h(n)*(ddth_joints(n-1,"end",j,s) + 4*ddth_joints(n,"mid",j,s) + ddth_joints(n,"end",j,s))/6;

*GROUND-------------------------------------------------------------------------
variables
foot_x(n,l,s)
foot_xvel(n,l,s)
;

positive variables
foot_height(n,l,s,sgn)
gamma(n,l,s)
groundA(n,l,s,gr)
groundB(n,l,s,gr)
ground_var(n,l,s,gr)
ground_violation(n,l,s,gr)
ground_penalty
;

$include "ground.txt";

Equations
get_foot_height(n,p,l,s)
Gr_contact_A(n,l,s)
Gr_contact_B(n,l,s)
Gr_friction_A(n,l,s)
Gr_friction_B(n,l,s)
Gr_slip_pos_A(n,l,s)
Gr_slip_pos_B(n,l,s)
Gr_slip_neg_A(n,l,s)
Gr_slip_neg_B(n,l,s)
get_ground_violation(n,l,s,gr)
ground_eq
;

Gr_contact_A(n,l,s)$feet(l).. groundA(n,l,s,"contact") =e= foot_height(n,l,s,"ps");
Gr_contact_B(n,l,s)$feet(l).. groundB(n,l,s,"contact") =e= GRF(n,"end",l,s,"y","ps");
Gr_friction_A(n,l,s)$feet(l).. groundA(n,l,s,"friction") =e= uground*GRF(n,"end",l,s,"y","ps") - GRF(n,"end",l,s,"x","ps") - GRF(n,"end",l,s,"x","ng");
Gr_friction_B(n,l,s)$feet(l).. groundB(n,l,s,"friction") =e= gamma(n,l,s);
Gr_slip_pos_A(n,l,s)$feet(l).. groundA(n,l,s,"slip_pos") =e= gamma(n,l,s) + foot_xvel(n,l,s);
Gr_slip_pos_B(n,l,s)$feet(l).. groundB(n,l,s,"slip_pos") =e= GRF(n,"end",l,s,"x","ps");
Gr_slip_neg_A(n,l,s)$feet(l).. groundA(n,l,s,"slip_neg") =e= gamma(n,l,s) - foot_xvel(n,l,s);
Gr_slip_neg_B(n,l,s)$feet(l).. groundB(n,l,s,"slip_neg") =e= GRF(n,"end",l,s,"x","ng");

get_ground_violation(n,l,s,gr)$feet(l).. ground_violation(n,l,s,gr) =e= groundA(n,l,s,gr)*groundB(n,l,s,gr);

ground_eq.. ground_penalty =e= sum((n,l,s,gr)$feet(l), groundA(n,l,s,gr)*groundB(n,l,s,gr));

GRF.up(n,p,l,s,"y","ps") = 10*g*m_body;
*JOINT XY-----------------------------------------------------------------------
variables
x_joints(n,j,s)

positive variables
y_joints(n,j,s,sgn);

$include "joint_xy.txt";

y_joints.fx(n,j,s,"ng") = 0;
foot_height.fx(n,l,s,"ng") = 0;
*TAIL---------------------------------------------------------------------------
positive variables
tail_velocity(n,sgn)
;

Equations
get_tail_velocity(n,sgn)
;

get_tail_velocity(n,sgn).. tail_velocity(n,"ps") - tail_velocity(n,"ng") =e= dq_t(n,"end","th_tail");
$include "tail_y.txt";
*STARTING POINT-----------------------------------------------------------------
parameter
q_t_start(i)
q_start(j,s)
dq_t_start(i)
dq_start(j,s)
;

Equations
starting_q_t(i)
starting_dq_t(i)
starting_th_up(j,s)
starting_th_lo(j,s)
starting_dth(j,s)
;

starting_q_t(i)$(not sameas(i,"th_tail")).. q_t("node1","end",i) =e= q_t_start(i);
starting_dq_t(i)$(not sameas(i,"th_tail")) .. dq_t("node1","end",i) =e= dq_t_start(i);
starting_th_up(j,s).. th_joints("node1","end",j,s) =l= q_start(j,s)+0.1;
starting_th_lo(j,s).. th_joints("node1","end",j,s) =g= q_start(j,s)-0.1;
starting_dth(j,s).. dth_joints("node1","end",j,s) =e= dq_start(j,s);

q_t.fx("node1","end","th_tail") = 0;
dq_t.fx("node1","end","th_tail") = 0;

$include "starting_point.txt";

*END CONDITIONS-----------------------------------------------------------------
positive variables
end_dx(n,sgn)
end_dth(n,sgn)
end_ddx(n,sgn)
end_ddth(n,sgn)
end_foot_height(n);

Equations
stopping_dx(n)
stopping_dth(n)
stopping_ddx(n)
stopping_ddth(n)
stopping_feet(n)
;

stopping_dx(n)$(ord(n) gt card(n)-5).. end_dx(n,"ps") - end_dx(n,"ng") =e= dq_t(n,"end","x");
stopping_dth(n)$(ord(n) gt card(n)-5).. end_dth(n,"ps") - end_dth(n,"ng") =e= dq_t(n,"end","th");
stopping_ddx(n)$(ord(n) gt card(n)-5).. end_ddx(n,"ps") - end_ddx(n,"ng") =e= ddq_t(n,"end","x");
stopping_ddth(n)$(ord(n) gt card(n)-5).. end_ddth(n,"ps") - end_ddth(n,"ng") =e= ddq_t(n,"end","th");
stopping_feet(n)$(ord(n) gt card(n)-5).. end_foot_height(n) =e= sum(s, foot_height(n,"forefoot",s,"ps") + foot_height(n,"hindfoot",s,"ps"));

end_dx.fx(n,"ps")$(ord(n) eq card(n)) = 0;
end_foot_height.fx(n)$(ord(n) eq card(n)) = 0;
*COST FUNCTION------------------------------------------------------------------
scalar ro /1/;

variable
Penalty
objective
Jcost;

Equation
Penalty_eq
objective_eq
cost;

Penalty_eq.. Penalty =e= 0
+sum((n)$(ord(n) gt card(n)-5), end_dx(n,"ps"))
+sum((n)$(ord(n) gt card(n)-5), end_dth(n,"ng"))
+sum((n)$(ord(n) gt card(n)-5), end_ddx(n,"ps"))
+sum((n)$(ord(n) gt card(n)-5), end_ddth(n,"ng"))
+sum((n)$(ord(n) gt card(n)-5), end_foot_height(n))

+ground_penalty
+(1-tail_bool)*sum((n,sgn), tail_velocity(n,sgn))
;

objective_eq.. objective =e= sum((n)$(ord(n) gt 1), power(h(n)*dq_t(n,"end","x"),2)) ;

cost.. Jcost =e= objective + ro*Penalty;

* define model
model quad /all/;

quad.reslim = 20000;
quad.workfactor = 10;
quad.threads = 4;
quad.limrow = 20;
*quad.optfile = 1;

option nlp = conopt;

file results /results.csv/ ;
results.nd = 8; results.pw = 10000;

*RANDOMIZE----------------------------------------------------------------------
execseed = 1 + gmillisec(jnow);

loop (seed,

loop(n,
q_t.l(n,p,"x") = uniform(0,3);
q_t.l(n,p,"y") = uniform(0,2);
q_t.l(n,p,"th") = uniform(-pi/3,pi/3);
q_t.l(n,p,"th_tail") = uniform(-pi/2,pi/2);

th_joints.l(n,p,j,s) = uniform(-pi/6,pi/6);
);

dq_t.l(n,p,i) = 0.01;
dth_joints.l(n,p,j,s) = 0.01;
ddq_t.l(n,p,i) = 0.01;
ddth_joints.l(n,p,j,s) = 0.01;

tau.l(n,p,j,s) = 0.01;
tau_tail.l(n,p) = 0.01;

GRF.l(n,p,l,s,"y",sgn) = g*m_body;
GRF.l(n,p,l,s,"x","ps") = uground*g*m_body;
GRF.l(n,p,l,s,"y","ng") = 0.01;

trig_t.l(n,p,i,tr) = 0.01;
trig.l(n,p,j,s,tr) = 0.01;

foot_x.l(n,l,s) = 0.01;
foot_xvel.l(n,l,s) = 0.01;
foot_height.l(n,l,s,sgn) = 0.01;
gamma.l(n,l,s) = 0.01;

groundA.l(n,l,s,"contact") = 0.01;
groundB.l(n,l,s,"contact") = 0.01;
groundA.l(n,l,s,"friction") = 0.01;
groundB.l(n,l,s,"friction") = 0.01;
groundA.l(n,l,s,"slip_pos") = 0.01;
groundB.l(n,l,s,"slip_pos") = 0.01;
groundA.l(n,l,s,"slip_neg") = 0.01;
groundB.l(n,l,s,"slip_neg") = 0.01;

x_joints.l(n,j,s) = 0.01;
y_joints.l(n,j,s,sgn) = 0.01;

tail_velocity.l(n,sgn) = 0.01;

*SOLVE--------------------------------------------------------------------------
solve quad using NLP minimizing Jcost;

*OUTPUT-------------------------------------------------------------------------
if(ground_penalty.l <= 1E-3 and penalty.l <= 0.1,
put results;

if(tail_bool eq 1,
put_utilities 'ren' / 'results\limtail_'friction.tl:0 point:<2:0'_'seed.tl:0'.csv';
)
if(tail_bool eq 0,
put_utilities 'ren' / 'results\limnotail_'friction.tl:0 point:<2:0'_'seed.tl:0'.csv';
)

*put objective.l";" Penalty.l";"/;

put
h.l("node1")";"
q_t.l("node1","end","x")";" q_t.l("node1","end","y")";" q_t.l("node1","end","th")";"
th_joints.l("node1","end","shoulder","R")";" th_joints.l("node1","end","elbow","R")";"  th_joints.l("node1","end","hip","R")";"  th_joints.l("node1","end","knee","R")";"
q_t.l("node1","end","th_tail")";"
dq_t.l("node1","end","x")";" dq_t.l("node1","end","y")";" dq_t.l("node1","end","th")";"
dth_joints.l("node1","end","shoulder","R")";" dth_joints.l("node1","end","elbow","R")";"  dth_joints.l("node1","end","hip","R")";"  dth_joints.l("node1","end","knee","R")";"
dq_t.l("node1","end","th_tail")";"
tau.l("node1","end","shoulder","R")";" tau.l("node1","end","elbow","R")";" tau.l("node1","end","hip","R")";" tau.l("node1","end","knee","R")";"
tau_tail.l("node1","end")";"
GRF.l("node1","end","forefoot","R","x","ps")";" GRF.l("node1","end","forefoot","R","x","ng")";" GRF.l("node1","end","forefoot","R","y","ps")";"
GRF.l("node1","end","hindfoot","R","x","ps")";" GRF.l("node1","end","hindfoot","R","x","ng")";" GRF.l("node1","end","hindfoot","R","y","ps")";"
/;

loop (n$(ord(n) gt 1),
put
h.l(n)";"
q_t.l(n,"mid","x")";" q_t.l(n,"mid","y")";" q_t.l(n,"mid","th")";"
th_joints.l(n,"mid","shoulder","R")";" th_joints.l(n,"mid","elbow","R")";"  th_joints.l(n,"mid","hip","R")";"  th_joints.l(n,"mid","knee","R")";"
q_t.l(n,"mid","th_tail")";"
dq_t.l(n,"mid","x")";" dq_t.l(n,"mid","y")";" dq_t.l(n,"mid","th")";"
dth_joints.l(n,"mid","shoulder","R")";" dth_joints.l(n,"mid","elbow","R")";"  dth_joints.l(n,"mid","hip","R")";"  dth_joints.l(n,"mid","knee","R")";"
dq_t.l(n,"mid","th_tail")";"
tau.l(n,"mid","shoulder","R")";" tau.l(n,"mid","elbow","R")";" tau.l(n,"mid","hip","R")";" tau.l(n,"mid","knee","R")";"
tau_tail.l(n,"mid")";"
GRF.l(n,"mid","forefoot","R","x","ps")";" GRF.l(n,"mid","forefoot","R","x","ng")";" GRF.l(n,"mid","forefoot","R","y","ps")";"
GRF.l(n,"mid","hindfoot","R","x","ps")";" GRF.l(n,"mid","hindfoot","R","x","ng")";" GRF.l(n,"mid","hindfoot","R","y","ps")";"
/;

put
h.l(n)";"
q_t.l(n,"end","x")";" q_t.l(n,"end","y")";" q_t.l(n,"end","th")";"
th_joints.l(n,"end","shoulder","R")";" th_joints.l(n,"end","elbow","R")";"  th_joints.l(n,"end","hip","R")";"  th_joints.l(n,"end","knee","R")";"
q_t.l(n,"end","th_tail")";"
dq_t.l(n,"end","x")";" dq_t.l(n,"end","y")";" dq_t.l(n,"end","th")";"
dth_joints.l(n,"end","shoulder","R")";" dth_joints.l(n,"end","elbow","R")";"  dth_joints.l(n,"end","hip","R")";"  dth_joints.l(n,"end","knee","R")";"
dq_t.l(n,"mid","th_tail")";"
tau.l(n,"end","shoulder","R")";" tau.l(n,"end","elbow","R")";" tau.l(n,"end","hip","R")";" tau.l(n,"end","knee","R")";"
tau_tail.l(n,"end")";"
GRF.l(n,"end","forefoot","R","x","ps")";" GRF.l(n,"end","forefoot","R","x","ng")";" GRF.l(n,"end","forefoot","R","y","ps")";"
GRF.l(n,"end","hindfoot","R","x","ps")";" GRF.l(n,"end","hindfoot","R","x","ng")";" GRF.l(n,"end","hindfoot","R","y","ps")";"
/;
);
);
putclose;
);

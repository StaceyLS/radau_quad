sets n "colocation elements" /node1 * node25/
     p "point" /p1*p5/
     i "trunk coordinates" /x,y,th,th_tail/
     i_angles(i) /th,th_tail/
     j "joints" /shoulder,elbow,hip,knee/
     j1(j) /shoulder,hip/
     j2(j) /elbow,knee/
     jf(j) /shoulder,elbow/
     jh(j) /hip,knee/
     l "limbs" /upperarm,forearm,forefoot,thigh,calf,hindfoot/
     l2(l) /forearm,calf/
     feet(l) "feet" /forefoot,hindfoot/
     s "sides" /R,L/
     sgn "sign" /ps,ng/
     gr /contact,friction,slip_pos,slip_neg/
     end /A,B/
     tr "trig functions" /sin,cos/

     seed /s1*s1/;

singleton set friction /high/;
scalar point /3/
tail_bool /1/;

*Quadrature parameters----------------------------------------------------------
parameters
int(p) "time interval of point within element";

*int("p1") = 0.5;
*int("p2") = 1;

int("p1") = 0.5 - sqrt(7 + 2*sqrt(7))/42;
int("p2") = 0.5 - sqrt(7 - 2*sqrt(7))/42;
int("p3") = 0.5 + sqrt(7 - 2*sqrt(7))/42;
int("p4") = 0.5 + sqrt(7 + 2*sqrt(7))/42;
int("p5") = 1;

Table  omega(p,p)  Lobatto matrix
*         p1      p2
*p1       0.75    -0.25
*p2       1       0;

         p1      p2      p3      p4      p5
p1       0.191   -0.147  0.139   -0.113  0.047
p2       0.276   0.059   0.051   -0.050  0.022
p3       0.267   0.193   0.251   -0.114  0.045
p4       0.269   0.178   0.384   0.032   0.019
p5       0.269   0.181   0.374   0.110   0.067
;
*model parameters---------------------------------------------------------------
Scalars
g /9.81/
uground /1.2/
pi /3.14159265359/

m_t /4.5/
len_t /0.5/
clen_t /0.375/
In_t
m_body

m_tail /0.5/
len_tail /0.5/
clen_tail
In_tail
;

In_t = m_t*(len_t**2)/12;
clen_tail = len_tail/2;
In_tail = m_tail*(len_tail**2)/12;

Parameters
m(l) "mass" /upperarm 1.5, forearm 1.5, forefoot 0, thigh 1.5, calf 1.5, hindfoot 0/
len(l) "length" /upperarm 0.2, forearm 0.25, forefoot 0, thigh 0.2, calf 0.3, hindfoot 0/
clen(l)
In(l)
;

clen(l) = len(l)/2;
In(l) = m(l)*(len(l)**2)/12;
m_body = m_t + m_tail + card(s)*sum(l, m(l));

variables
q_t(n,p,i) "torso coordinates"
dq_t(n,p,i)
ddq_t(n,p,i)

th_joints(n,p,j,s) "joint angles"
dth_joints(n,p,j,s) "joint angles"
ddth_joints(n,p,j,s)

tau(n,p,j,s)
tau_tail(n,p);

positive variables
h_global
h(n)
GRF(n,p,l,s,i,sgn)
;

h.up(n) = 0.04;
h.lo(n) = 0.001;
h.l(n) = 0.01;

scalars
Tmax /56/
w_noload /68/
;

*angle bounds-------------------------------------------------------------------
q_t.up(n,p,"th")$(ord(p) eq card(p)) = pi/2;
q_t.lo(n,p,"th")$(ord(p) eq card(p)) = -pi/2;

q_t.up(n,p,"th_tail")$(ord(p) eq card(p)) = pi/2;
q_t.lo(n,p,"th_tail")$(ord(p) eq card(p)) = -pi;

th_joints.up(n,p,"hip",s)$(ord(p) eq card(p)) = pi/2;
th_joints.lo(n,p,"hip",s)$(ord(p) eq card(p)) = -pi/2;

th_joints.up(n,p,"shoulder",s)$(ord(p) eq card(p)) = pi/2;
th_joints.lo(n,p,"shoulder",s)$(ord(p) eq card(p)) = -pi/2;

th_joints.up(n,p,"knee",s)$(ord(p) eq card(p)) = 0;
th_joints.lo(n,p,"knee",s)$(ord(p) eq card(p)) = -pi/2;

th_joints.up(n,p,"elbow",s)$(ord(p) eq card(p)) = pi/2;
th_joints.lo(n,p,"elbow",s)$(ord(p) eq card(p)) = 0;

*TIME---------------------------------------------------------------------------
Equations
time_min(n)
time_max(n)
;

time_min(n).. h(n) =g= 0.8*h_global;
time_max(n).. h(n) =l= 1.2*h_global;

variables
time(n,p);

Equations
get_true_time1(n)
get_true_time2(n,p);

get_true_time1(n).. time(n,"p1") =e= h(n)*int("p1");
get_true_time2(n,p)$(ord(p) gt 1).. time(n,p) =e= h(n)*(int(p)-int(p-1));

*MOTOR MODEL--------------------------------------------------------------------
Equations
tau_min(n,p,j,s)
tau_max(n,p,j,s)

tau_tail_min(n,p)
tau_tail_max(n,p)

;

tau_min(n,p,j,s)$(ord(n) gt 1 or ord(p) eq card(p)).. tau(n,p,j,s) =g= -Tmax - (Tmax/w_noload)*dth_joints(n,p,j,s);
tau_max(n,p,j,s)$(ord(n) gt 1 or ord(p) eq card(p)).. tau(n,p,j,s) =l= Tmax - (Tmax/w_noload)*dth_joints(n,p,j,s);

tau_tail_min(n,p)$(ord(n) gt 1 or ord(p) eq card(p)).. tau_tail(n,p) =g= -Tmax - (Tmax/w_noload)*dq_t(n,p,"th_tail");
tau_tail_max(n,p)$(ord(n) gt 1 or ord(p) eq card(p)).. tau_tail(n,p) =l= Tmax - (Tmax/w_noload)*dq_t(n,p,"th_tail");

tau.up(n,p,j,s) = Tmax;
tau.lo(n,p,j,s) = -Tmax;

tau_tail.up(n,p) = Tmax;
tau_tail.lo(n,p) = -Tmax;

*SIN AND COS--------------------------------------------------------------------
variables
trig_t(n,p,i,tr)
trig(n,p,j,s,tr)
;

Equations
get_sin_t(n,p,i)
get_cos_t(n,p,i)
get_sin(n,p,j,s)
get_cos(n,p,j,s)

;

get_sin_t(n,p,i)$((ord(n) gt 1 or ord(p) eq card(p)) and i_angles(i)).. trig_t(n,p,i,"sin") =e= q_t(n,p,i) - power(q_t(n,p,i),3)/fact(3) + power(q_t(n,p,i),5)/fact(5) - power(q_t(n,p,i),7)/fact(7)
                                                         + power(q_t(n,p,i),9)/fact(9) - power(q_t(n,p,i),11)/fact(11) + power(q_t(n,p,i),13)/fact(13)
                                                         - power(q_t(n,p,i),15)/fact(15) + power(q_t(n,p,i),17)/fact(17) - power(q_t(n,p,i),19)/fact(19);
get_cos_t(n,p,i)$((ord(n) gt 1 or ord(p) eq card(p)) and i_angles(i)).. trig_t(n,p,i,"cos") =e= 1 - power(q_t(n,p,i),2)/fact(2) + power(q_t(n,p,i),4)/fact(4) - power(q_t(n,p,i),6)/fact(6)
                                                         + power(q_t(n,p,i),8)/fact(8) - power(q_t(n,p,i),10)/fact(10) + power(q_t(n,p,i),12)/fact(12)
                                                         - power(q_t(n,p,i),14)/fact(14) + power(q_t(n,p,i),16)/fact(16) - power(q_t(n,p,i),18)/fact(18);

get_sin(n,p,j,s)$(ord(n) gt 1 or ord(p) eq card(p)).. trig(n,p,j,s,"sin") =e= th_joints(n,p,j,s) - power(th_joints(n,p,j,s),3)/fact(3) + power(th_joints(n,p,j,s),5)/fact(5) - power(th_joints(n,p,j,s),7)/fact(7)
                                                         + power(th_joints(n,p,j,s),9)/fact(9) - power(th_joints(n,p,j,s),11)/fact(11) + power(th_joints(n,p,j,s),13)/fact(13)
                                                         - power(th_joints(n,p,j,s),15)/fact(15) + power(th_joints(n,p,j,s),17)/fact(17) - power(th_joints(n,p,j,s),19)/fact(19);
get_cos(n,p,j,s)$(ord(n) gt 1 or ord(p) eq card(p)).. trig(n,p,j,s,"cos") =e= 1 - power(th_joints(n,p,j,s),2)/fact(2) + power(th_joints(n,p,j,s),4)/fact(4) - power(th_joints(n,p,j,s),6)/fact(6)
                                                         + power(th_joints(n,p,j,s),8)/fact(8) - power(th_joints(n,p,j,s),10)/fact(10) + power(th_joints(n,p,j,s),12)/fact(12)
                                                         - power(th_joints(n,p,j,s),14)/fact(14) + power(th_joints(n,p,j,s),16)/fact(16) - power(th_joints(n,p,j,s),18)/fact(18);

*DYNAMICS-----------------------------------------------------------------------
$include "dynamics.txt";

alias(p,pp)

Equations
interp_q_t(n,p,i)
interp_dq_t(n,p,i)

interp_th_joints(n,p,j,s)
interp_dth_joints(n,p,j,s)

interp_tau(n,p,j,s)
interp_tau_tail(n,p)

interp_GRF_x(n,p,l,s,sgn)
interp_GRF_y(n,p,l,s)
;

interp_q_t(n,p,i)$(ord(n) gt 1).. q_t(n,p,i) =e= sum(pp$(ord(pp) eq card(pp)), q_t(n-1,pp,i)) + h(n)*sum(pp, omega(p,pp)*dq_t(n,pp,i));
interp_dq_t(n,p,i)$(ord(n) gt 1).. dq_t(n,p,i) =e= sum(pp$(ord(pp) eq card(pp)), dq_t(n-1,pp,i)) + h(n)*sum(pp, omega(p,pp)*ddq_t(n,pp,i));

interp_th_joints(n,p,j,s)$(ord(n) gt 1).. th_joints(n,p,j,s) =e= sum(pp$(ord(pp) eq card(pp)), th_joints(n-1,pp,j,s)) + h(n)*sum(pp, omega(p,pp)*dth_joints(n,pp,j,s));
interp_dth_joints(n,p,j,s)$(ord(n) gt 1).. dth_joints(n,p,j,s) =e= sum(pp$(ord(pp) eq card(pp)), dth_joints(n-1,pp,j,s)) + h(n)*sum(pp, omega(p,pp)*ddth_joints(n,pp,j,s));

interp_tau(n,p,j,s)$(ord(n) gt 1 and ord(p) lt card(p)).. tau(n,p,j,s) =e= sum(pp$(ord(pp) eq card(pp)),tau(n,pp,j,s) - tau(n-1,pp,j,s))*int(p) + sum(pp$(ord(pp) eq card(pp)),tau(n-1,pp,j,s));
interp_tau_tail(n,p)$(ord(n) gt 1 and ord(p) lt card(p)).. tau_tail(n,p) =e= sum(pp$(ord(pp) eq card(pp)),tau_tail(n,pp) - tau_tail(n-1,pp))*int(p) + sum(pp$(ord(pp) eq card(pp)),tau_tail(n-1,pp));

interp_GRF_x(n,p,l,s,sgn)$(ord(n) gt 1 and ord(p) lt card(p) and feet(l)).. GRF(n,p,l,s,"x",sgn) =e= sum(pp$(ord(pp) eq card(pp)),GRF(n,pp,l,s,"x",sgn) - GRF(n-1,pp,l,s,"x",sgn))*int(p) + sum(pp$(ord(pp) eq card(pp)),GRF(n-1,pp,l,s,"x",sgn));
interp_GRF_y(n,p,l,s)$(ord(n) gt 1 and ord(p) lt card(p) and feet(l)).. GRF(n,p,l,s,"y","ps") =e= sum(pp$(ord(pp) eq card(pp)),GRF(n,pp,l,s,"y","ps") - GRF(n-1,pp,l,s,"y","ps"))*int(p) + sum(pp$(ord(pp) eq card(pp)),GRF(n-1,pp,l,s,"y","ps"));

*GROUND-------------------------------------------------------------------------
variables
foot_xvel(n,p,l,s);

positive variables
foot_height(n,p,l,s,sgn)
gamma(n,p,l,s)
groundA(n,p,l,s,gr)
groundB(n,p,l,s,gr)
ground_var(n,p,l,s,gr)
ground_violation(n,p,l,s,gr)
ground_penalty
;

$include "ground.txt";

Equations
get_foot_height(n,p,l,s)
Gr_contact_A(n,p,l,s)
Gr_contact_B(n,p,l,s)
Gr_friction_A(n,p,l,s)
Gr_friction_B(n,p,l,s)
Gr_slip_pos_A(n,p,l,s)
Gr_slip_pos_B(n,p,l,s)
Gr_slip_neg_A(n,p,l,s)
Gr_slip_neg_B(n,p,l,s)
*get_ground_violation(n,l,s,gr)
ground_eq
;

Gr_contact_A(n,p,l,s)$feet(l).. groundA(n,p,l,s,"contact") =e= foot_height(n,p,l,s,"ps");
Gr_contact_B(n,p,l,s)$feet(l).. groundB(n,p,l,s,"contact") =e= GRF(n,p,l,s,"y","ps");
Gr_friction_A(n,p,l,s)$feet(l).. groundA(n,p,l,s,"friction") =e= uground*GRF(n,p,l,s,"y","ps") - GRF(n,p,l,s,"x","ps") - GRF(n,p,l,s,"x","ng");
Gr_friction_B(n,p,l,s)$feet(l).. groundB(n,p,l,s,"friction") =e= gamma(n,p,l,s);
Gr_slip_pos_A(n,p,l,s)$feet(l).. groundA(n,p,l,s,"slip_pos") =e= gamma(n,p,l,s) + foot_xvel(n,p,l,s);
Gr_slip_pos_B(n,p,l,s)$feet(l).. groundB(n,p,l,s,"slip_pos") =e= GRF(n,p,l,s,"x","ps");
Gr_slip_neg_A(n,p,l,s)$feet(l).. groundA(n,p,l,s,"slip_neg") =e= gamma(n,p,l,s) - foot_xvel(n,p,l,s);
Gr_slip_neg_B(n,p,l,s)$feet(l).. groundB(n,p,l,s,"slip_neg") =e= GRF(n,p,l,s,"x","ng");

*get_ground_violation(n,l,s,gr)$feet(l).. ground_violation(n,l,s,gr) =e= groundA(n,l,s,gr)*groundB(n,l,s,gr);

ground_eq.. ground_penalty =e= sum((n,p,l,s,gr)$(ord(p) eq card(p) and feet(l)), groundA(n,p,l,s,gr)*groundB(n,p,l,s,gr));

*JOINT XY-----------------------------------------------------------------------
positive variables
y_joints(n,p,j,s,sgn);

$include "joint_xy.txt";

y_joints.fx(n,p,j,s,"ng") = 0;
foot_height.fx(n,p,l,s,"ng") = 0;

*TAIL---------------------------------------------------------------------------
positive variables
tail_velocity(n,p,sgn)
;

Equations
get_tail_velocity(n,p,sgn)
;

get_tail_velocity(n,p,sgn).. tail_velocity(n,p,"ps") - tail_velocity(n,p,"ng") =e= dq_t(n,p,"th_tail");
$include "tail_y.txt";

*STARTING POINT-----------------------------------------------------------------
parameter
q_t_start(i)
q_start(j,s)
dq_t_start(i)
dq_start(j,s)
;

Equations
starting_q_t(p,i)
starting_dq_t(p,i)
starting_th(p,j,s)
starting_dth(p,j,s)
;

starting_q_t(p,i)$(ord(p) eq card(p) and not sameas(i,"th_tail")).. q_t("node1",p,i) =e= q_t_start(i);
starting_dq_t(p,i)$(ord(p) eq card(p) and not sameas(i,"th_tail")) .. dq_t("node1",p,i) =e= dq_t_start(i);
starting_th(p,j,s)$(ord(p) eq card(p)).. th_joints("node1",p,j,s) =e= q_start(j,s);
starting_dth(p,j,s)$(ord(p) eq card(p)).. dth_joints("node1",p,j,s) =e= dq_start(j,s);

q_t.fx("node1",p,"th_tail")$(ord(p) eq card(p)) = 0;
dq_t.fx("node1",p,"th_tail")$(ord(p) eq card(p)) = 0;
$include "starting_point.txt";

*END CONDITIONS-----------------------------------------------------------------
positive variables
end_dx(n,sgn)
end_dth(n,sgn)
end_ddx(n,sgn)
end_ddth(n,sgn)
end_foot_height(n);

Equations
stopping_dx(n,p)
stopping_dth(n,p)
stopping_ddx(n,p)
stopping_ddth(n,p)
stopping_feet(n,p)
;

stopping_dx(n,p)$(ord(n) eq card(n) and ord(p) eq card(p)).. end_dx(n,"ps") - end_dx(n,"ng") =e= dq_t(n,p,"x");
stopping_dth(n,p)$(ord(n) eq card(n) and ord(p) eq card(p)).. end_dth(n,"ps") - end_dth(n,"ng") =e= dq_t(n,p,"th");
stopping_ddx(n,p)$(ord(n) eq card(n) and ord(p) eq card(p)).. end_ddx(n,"ps") - end_ddx(n,"ng") =e= ddq_t(n,p,"x");
stopping_ddth(n,p)$(ord(n) eq card(n) and ord(p) eq card(p)).. end_ddth(n,"ps") - end_ddth(n,"ng") =e= ddq_t(n,p,"th");
stopping_feet(n,p)$(ord(n) eq card(n) and ord(p) eq card(p)).. end_foot_height(n) =e= sum(s, foot_height(n,p,"forefoot",s,"ps") + foot_height(n,p,"hindfoot",s,"ps"));

end_dx.fx(n,"ps")$(ord(n) eq card(n)) = 0;
end_foot_height.fx(n)$(ord(n) eq card(n)) = 0;

*COST FUNCTION------------------------------------------------------------------
scalar ro /1/;

variable
Penalty
objective
Jcost;

Equation
Penalty_eq
objective_eq
cost;

Penalty_eq.. Penalty =e= 0
+sum((n)$(ord(n) gt card(n)), end_dx(n,"ps"))
+sum((n)$(ord(n) gt card(n)), end_dth(n,"ng"))
+sum((n)$(ord(n) gt card(n)), end_ddx(n,"ps"))
+sum((n)$(ord(n) gt card(n)), end_ddth(n,"ng"))
+sum((n)$(ord(n) gt card(n)), end_foot_height(n))

+ground_penalty
+(1-tail_bool)*sum((n,p,sgn), tail_velocity(n,p,sgn))
;

objective_eq.. objective =e= 0;

cost.. Jcost =e= objective + ro*Penalty;

* define model
model quad /all/;

quad.reslim = 20000;
quad.workfactor = 10;
quad.threads = 4;
quad.limrow = 20;
*quad.optfile = 1;

option nlp = conopt;

file results /results.csv/ ;
results.nd = 8; results.pw = 10000;

*RANDOMIZE----------------------------------------------------------------------
execseed = 1 + gmillisec(jnow);

loop (seed,

loop(n,
q_t.l(n,p,"x") = uniform(0,3);
q_t.l(n,p,"y") = uniform(0,2);
q_t.l(n,p,"th") = uniform(-pi/3,pi/3);
q_t.l(n,p,"th_tail") = uniform(-pi/2,pi/2);

th_joints.l(n,p,j,s) = uniform(-pi/6,pi/6);
);

dq_t.l(n,p,i) = 0.01;
dth_joints.l(n,p,j,s) = 0.01;
ddq_t.l(n,p,i) = 0.01;
ddth_joints.l(n,p,j,s) = 0.01;

tau.l(n,p,j,s) = 0.01;
tau_tail.l(n,p) = 0.01;

GRF.l(n,p,l,s,"y","ps") = g*m_body;
GRF.l(n,p,l,s,"x",sgn) = uground*g*m_body;

trig_t.l(n,p,i,tr) = 0.01;
trig.l(n,p,j,s,tr) = 0.01;

foot_height.l(n,p,l,s,sgn) = 0.01;
gamma.l(n,p,l,s) = 0.01;

groundA.l(n,p,l,s,"contact") = 0.01;
groundB.l(n,p,l,s,"contact") = 0.01;
groundA.l(n,p,l,s,"friction") = 0.01;
groundB.l(n,p,l,s,"friction") = 0.01;
groundA.l(n,p,l,s,"slip_pos") = 0.01;
groundB.l(n,p,l,s,"slip_pos") = 0.01;
groundA.l(n,p,l,s,"slip_neg") = 0.01;
groundB.l(n,p,l,s,"slip_neg") = 0.01;

y_joints.l(n,p,j,s,sgn) = 0.01;

tail_velocity.l(n,p,sgn) = 0.01;

*SOLVE--------------------------------------------------------------------------
solve quad using NLP minimizing Jcost;

*OUTPUT-------------------------------------------------------------------------
if(1,
*ground_penalty.l <= 1E-3 and penalty.l <= 0.1,
put results;

*if(tail_bool eq 1,
*put_utilities 'ren' / 'results\lim2tail_'friction.tl:0 point:<2:0'_'seed.tl:0'.csv';
*)
*if(tail_bool eq 0,
*put_utilities 'ren' / 'results\lim2notail_'friction.tl:0 point:<2:0'_'seed.tl:0'.csv';
*)

*put objective.l";" Penalty.l";"/;

loop(p$(ord(p) eq card(p)),
put
time.l("node1",p)";"
q_t.l("node1",p,"x")";" q_t.l("node1",p,"y")";" q_t.l("node1",p,"th")";";
loop(s,
put th_joints.l("node1",p,"shoulder",s)";" th_joints.l("node1",p,"elbow",s)";"  th_joints.l("node1",p,"hip",s)";"  th_joints.l("node1",p,"knee",s)";" ;
);
put q_t.l("node1",p,"th_tail")";"
dq_t.l("node1",p,"x")";" dq_t.l("node1",p,"y")";" dq_t.l("node1",p,"th")";"
loop(s,
put dth_joints.l("node1",p,"shoulder",s)";" dth_joints.l("node1",p,"elbow",s)";"  dth_joints.l("node1",p,"hip",s)";"  dth_joints.l("node1",p,"knee",s)";" ;
);
put dq_t.l("node1",p,"th_tail")";";
loop(s,
put tau.l("node1",p,"shoulder",s)";" tau.l("node1",p,"elbow",s)";" tau.l("node1",p,"hip",s)";" tau.l("node1",p,"knee",s)";" ;
);
put tau_tail.l("node1",p)";";
loop(s,
put GRF.l("node1",p,"forefoot",s,"x","ps")";" GRF.l("node1",p,"forefoot",s,"x","ng")";" GRF.l("node1",p,"forefoot",s,"y","ps")";"
GRF.l("node1",p,"hindfoot",s,"x","ps")";" GRF.l("node1",p,"hindfoot",s,"x","ng")";" GRF.l("node1",p,"hindfoot",s,"y","ps")";" ;
);
put /;
);

loop (n$(ord(n) gt 1),
loop (p,
put
time.l(n,p)";"
q_t.l(n,p,"x")";" q_t.l(n,p,"y")";" q_t.l(n,p,"th")";"  ;
loop(s,
put th_joints.l(n,p,"shoulder",s)";" th_joints.l(n,p,"elbow",s)";"  th_joints.l(n,p,"hip",s)";"  th_joints.l(n,p,"knee",s)";"  ;
);
put q_t.l(n,p,"th_tail")";"
dq_t.l(n,p,"x")";" dq_t.l(n,p,"y")";" dq_t.l(n,p,"th")";" ;
loop(s,
put dth_joints.l(n,p,"shoulder",s)";" dth_joints.l(n,p,"elbow",s)";"  dth_joints.l(n,p,"hip",s)";"  dth_joints.l(n,p,"knee",s)";"  ;
);
put dq_t.l(n,p,"th_tail")";"
loop(s,
put tau.l(n,p,"shoulder",s)";" tau.l(n,p,"elbow",s)";" tau.l(n,p,"hip",s)";" tau.l(n,p,"knee",s)";";
);
put tau_tail.l(n,p)";";
loop(s,
put GRF.l(n,p,"forefoot",s,"x","ps")";" GRF.l(n,p,"forefoot",s,"x","ng")";" GRF.l(n,p,"forefoot",s,"y","ps")";"
GRF.l(n,p,"hindfoot",s,"x","ps")";" GRF.l(n,p,"hindfoot",s,"x","ng")";" GRF.l(n,p,"hindfoot",s,"y","ps")";"   ;
);
put /;
);

);
);
putclose;
);
